MSc Computer Science - Intelligent Systems
Name: Joshua Cassidy
Student Number: 20300057
Module: CS7IS3 INFORMATION RETRIEVAL AND WEB SEARCH

Access Instructions:
To access the AWS instance that the project is installed on used the run the command "sudo ssh -i information_retrieval_lucene_project_individual_assignment.pem ubuntu@63.32.95.132" (The "information_retrieval_lucene_project_individual_assignment.pem" file is attached to the submission of this project). Then use the cd command to change directory into the "lucene_project" where the project's code is stored. The "readme.md" file in the "lucene_project" folder contains instructions on how to install the projects dependencies and run the project. The GitLab repository for this project can be accessed through the following link: https://gitlab.com/cassidj3/continuous-assessment-part-one-cs7is3-information-retrieval-and-web-search/

Project Dependencies:
1. Java (Version 8 or greater)
2. Maven (Version 3.6.0)
3. Python3 (Note: ensure you have python mapped to the python3 environment variable and specific version of all the Python dependencies can be found in requirements.txt)
4. Matplotlib (Note: ensure that your operating system is able to use the TKAgg function)
5. pip (Note: ensure you have pip mapped to the pip3 environment variable)
6. Bash (Version 3.2)

Install the Projects dependencies
The following steps detail how to install the projects python dependencies:
1. Use the cd command in order to navigate to the directory where the project has been downloaded
2. Run the mvn package command to build the project and install the maven dependencies
3. Run the "pip3 install -r requirements.txt" command to install the project's python dependencies (Note: ensure that you are connected to the internet)

Running the Project
The following steps detail how to run the project:
1. Use the cd command in order to navigate to the directory where the project has been downloaded
2. To index documents run the command java -jar target/lucene_project-1.0-SNAPSHOT.jar -index index -data data/cran.all.1400 
3. To search documents run the command java -jar target/lucene_project-1.0-SNAPSHOT.jar -output output/BM25Similarity_Custom.txt -queriesFile data/cran.qry -index index
4. To get the projects usage parameters run the command java -jar target/lucene_project-1.0-SNAPSHOT.jar
5. Run the evaluate_configurations.sh (by using bash evaluate_configurations.sh command) to use each of the configurations and generate trec_eval results for those configurations
6. Run the python3 percision_recall_curves.py command to get the highest performing configuration in terms of map and a Recall-Precision Curve for the various analyzers that use the same scoring approach and the various scoring approaches that use the same analyzers

Note: In the analysis, it was discovered that the custom analyzer using BM25 scoring was the most effective configuration in the for searching the Cranfield dataset.
The BM25 model that used a custom-defined analyser achieving a MAP score of 0.365, a recall score of 0.299 and 0.405 for retrieval of the first 5 and 10 documents respectively. For the first 5 and 10 documents respectively and a precision score of 0.390 and 0.275 for the first 5 and 10 documents respectively.
To index documents with only this model run the command: bash index_documents.sh (This will index the documents using BM25 scoring approach and a custom defined analyser)
To search documents with only this model run the command: bash search_documents.sh (This will search the documents using BM25 scoring approach and a custom defined analyser)

Note: 
the output folder contains the query results of the search engine
the trec_eval_results folder contains the trec_eval results of the queries
run sudo apt-get install openjdk-8-jdk if there are compilation issues with compiling the maven project